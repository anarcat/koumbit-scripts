#!/bin/bash

RTUSER=""
GETCONFIRMATION=1

function usage {
    echo "Usage: $(basename $0) [-h] [-y] -u USER"
    echo -e "\t-u user@example.com\tThe e-mail of the owner to de-assign tickets from"
    echo -e "\t-y\t\t\tProceed without confirmation"
    echo -e "\t-h\t\t\tShow this message"
}

while getopts ":hyu:" arg ; do
  case ${arg} in
      y)
          GETCONFIRMATION=0
          ;;
      h)
          usage
          exit 1
          ;;
      u)
          RTUSER="${OPTARG}"
          ;;
      ?)
          echo "Invalid argument: ${arg}"
          usage
          exit 1
          ;;
  esac
done

if [ -z "${RTUSER}" ] ; then
  echo "Need a user"
  usage
  exit 1
fi

ticket_query="owner = ${RTUSER}"
RT_TICKETS=$(rt ls -t ticket -i "${ticket_query}" | sed 's#ticket/##g')

if [ ${GETCONFIRMATION} == "1" ] ; then
    rt ls -t ticket -s -f subject "${ticket_query}"
    echo ""
    LINE_COUNT=$(echo "${RT_TICKETS}" | wc -l)
    echo "Found ${LINE_COUNT} tickets assigned to ${RTUSER}"
    read -r -p "Proceed with un-assigning from the aove list? [Y/n] " proceed
    if [[ ! $proceed =~ ^(y|Y|)$ ]] ; then
        echo aborting
        exit 1
    fi
fi

# There is a bug when this is run in the rt4-clients I guess
# Use of uninitialized value $list in split at /usr/bin/rt line 1642, <STDIN> line 1.
# So avoid this (easier) command for the moment
#echo ${RT_TICKETS} | rt edit -t ticket set owner=nobody -

# If the "set owner=nobody" is quoted, then the result is more like
# rt: Invalid object ID specified: '3 14 15 17 26 ....'

for i in ${RT_TICKETS} ; do
    rt edit -t ticket set owner=nobody "$i"
done
