#!/bin/bash
#
# Move tickets to the spam queue
#
#
function usage()
{
  echo "usage: $0 <RT_TICKET_ID>..." >&2
  echo >&2
  echo "Mark given tickets in RT as spam." >&2
  echo "Multiple ticket IDs can be specified" >&2
  echo >&2

  exit 1
}


while [ $# -gt 0 ] ; do
  case "$1" in
    -*) usage;;
    *) RT_TICKET="$1 $RT_TICKET";;
  esac
  shift
done

if [ -z "$RT_TICKET" ] ; then
  usage
fi

for i in $RT_TICKET; do
  rt edit ticket/$i set status=rejected queue=Spam
done
