#!/usr/bin/python3
"""Close tickets without leaving a comments in them.

Usage:
  quick-resolve.py [<ticket_number> ...]

Options:
  -h, --help: print this help text

If no ticket number is given, the list of currently open tickets will be
displayed and you will be able to interactively choose which ones to mark as
spam.
"""

import sys


from support_rt.connect import connect_to_rt
from support_rt.tickets import get_support_tickets, choose_tickets


if __name__ == '__main__':
    if "-h" in sys.argv or "--help" in sys.argv:
        print(__doc__)
        exit(0)

    try:
        tickets = [int(x) for x in sys.argv[1:]]
    except ValueError:
        print("error: All arguments should be integer numbers",
              file=sys.stderr)
        exit(1)

    try:
        tracker = connect_to_rt()
    except Exception:
        print("Failed to connect to RT")
        exit(1)

    if not len(tickets):
        qprompt = "Which tickets should get closed?"
        tickets = choose_tickets(get_support_tickets(tracker), message=qprompt)

    for ticket in tickets:
        print("Closing ticket {}".format(ticket))
        tracker.ticket.update(
            ticket,
            {
                "Status": "resolved"
            })
