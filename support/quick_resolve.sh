#!/bin/bash
#
# This is really just a wrapper to a super easy rt cli command. But it lets you
# supply more than one ticket to close them all.
# The name of the script is the name of the same action in the web interface.
#
RT_TICKET="$@"

if [ -z "$RT_TICKET" ]; then
  echo "usage: $0 <RT_TICKET_ID>..." >&2
  echo >&2
  echo "Close tickets without commenting about why they get resolved." >&2
  echo "Multiple ticket IDs can be specified" >&2
  echo >&2

  exit 1
fi

for i in $RT_TICKET; do
  rt res "$i"
done
