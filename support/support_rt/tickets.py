"""Search for open support tickets or interactively choose them."""

import logging


from rt_client.v2.client import Client
import inquirer


SUPPORT_QUEUES = [
        'CSN-MonSyndicat',
        'Comptes',
        'Info',
        'Support',
        'Support2',
        'NOC',
    ]

OPEN_STATUSES = [
        'new',
        'open',
    ]


def get_support_tickets(
        tracker, queues=SUPPORT_QUEUES,
        statuses=OPEN_STATUSES, owners=['Nobody'],
        fields=["Subject", "Queue", "Requestor"]):
    """Get tickets from all support queues."""
    if not isinstance(tracker, Client):
        raise ValueError(
            "Object 'tracker' should be of class rt_client.v2.client.Client")

    q = [f"Queue='{x}'" for x in queues]
    s = [f"Status='{x}'" for x in statuses]
    o = [f"Owner='{x}'" for x in owners]

    query = " AND ".join([
        f"({' OR '.join(q)})",
        f"({' OR '.join(s)})",
        f"({' OR '.join(o)})",
    ])

    results = tracker.ticket.search(query, fields=fields)["items"]
    logging.debug("ticket structure: %s", results[0])

    # The "tickets" endpoint is buggy (still in RT 5.0) and returns an empty
    # string instead of the list of requestors. To get the information we need
    # to request information about each ticket individually.
    # This unfortunately makes the code pretty slow since we're doing lots
    # more HTTP requests and waiting for each answer.
    if "Requestor" in fields:
        fields.remove("Requestor")
        for i in range(0, len(results)):
            # YOLO not checking for exceptions
            ticket_info = tracker.sess.get(results[i]["_url"]).json()
            results[i]["Requestor"] = [
                x["id"] for x in ticket_info["Requestor"]]

    queue_cache = {}
    if "Queue" in fields:
        for i in range(0, len(results)):
            queue_url = results[i]["Queue"]["_url"]
            logging.debug("found ticket %s in queue url: %s",
                          results[i]["id"], queue_url)

            queue_info = queue_cache.get(queue_url, None)
            if queue_info is None:
                logging.debug("queue still unknown, querying API.")
                # YOLO not checking for exceptions here
                queue_info = tracker.sess.get(queue_url).json()
                logging.debug("got queue info: %s", queue_info)

                queue_cache[queue_url] = queue_info

            results[i]["Queue"] = queue_info["Name"]

    return results


def remove_prefix(text, prefix):
    """Remove a certain prefix from a string if it is present."""
    return text[text.startswith(prefix) and len(prefix):]


def choose_tickets(tickets,
                   message="Please choose among the following tickets"):
    """Prompt the user to choose tickets among a list.

    tickets should be a list of dictionaries, each containing at least a key
    "id" and a key "Subject".

    A list of ticket IDs that were chosen by the user will be returned. The
    type of object gets stripped from the start of the "id" value. This will
    make it easier to use the ticket numbers for modifying them.
    """
    if not len(tickets):
        # nothing to choose from, let's not bother the user
        return []

    choices = [
        {
            "subject": t["Subject"],
            "queue": t.get("Queue", ":unknown:"),
            "requestors": t.get("Requestor", [":not set:"]),
            "value": remove_prefix(t["id"], "ticket/"),
        } for t in tickets]

    questions = [
        inquirer.Checkbox(
            name="tickets", message=message, choices=choices, carousel=True)
    ]
    # XXX This causes some ugly in the terminal
    # See: https://github.com/magmax/python-inquirer/issues/80
    # See: https://github.com/magmax/python-inquirer/issues/115
    answers = inquirer.prompt(questions)

    if answers:
        return [x["value"] for x in answers["tickets"]]

    return []


if __name__ == '__main__':
    # Running this as a "main" program means we're testing the code out, so we
    # want to have debugging output.
    logging.basicConfig(level=logging.DEBUG)

    from connect import connect_to_rt

    logging.debug("opening connection to RT")
    tracker = connect_to_rt()

    logging.debug("fetching support tickets")
    tickets = get_support_tickets(tracker)
    print(repr(tickets))

    logging.debug("showing UI for choosing tickets")
    choices = choose_tickets(tickets)
    if len(choices):
        print(type(choices[0]))
    print(repr(choices))
