"""Just trying out the rt lib."""

import os
import getpass
import logging

from requests.exceptions import RequestException

from rt_client import client


KOUMBIT_RT = 'https://rt.koumbit.net/rt/'


class ConnectionError(Exception):
    """Login to RT was not possible for some reason."""

    pass


# The RT lib hasn't implemented merging tickets. We'll implement the
# functionality ourselves here.. hopefully at some point in the future this
# gets upstreamed (or if there's a better lib that has the feature we'll
# change to using it).
def merge(self, ticket_id, merge_into):
    """Merge ticket_id into merge_into.

    Both ticket_id and merge_into should be integer numbers representing
    ticket IDs.
    """
    # TODO: I couldn't find a way in REST2 to merge to tickets.. but maybe
    # with luck we can use the internal function through a PUT on a ticket
    # with 'MergeInto' as the attribute name for the ticket. need to try if
    # that works or not.
    logging.error("Unfortunately, merging is still unimplemented!")
    logging.error("No change was applied to tickets")


# Monkey-patch the merge method into the class to add the functionality
client.client_v2.TicketManager.merge = merge


# TODO Wrap this into a class with all ticket search functions. This should
# become an abstraction of the underlying RT library.
def connect_to_rt(user_name=None):
    """Prepare a REST API connection to Koumbit's RT.

    This will try to reuse the username defined in ~/.rt_sessions. Otherwise
    it will prompt for the username. If an empty user name is entered on the
    prompt, a ValueError exception is raised.

    A prompt for the password will always show up.

    Returns an rt.Rt object after login was completed. Ticket searches and
    other actions can then be performed on this object.

    If login fails, a LoginFailure exception is raised.
    """
    if not user_name:
        try:
            sessions_path = os.path.expanduser("~/.rt_sessions")
            with open(sessions_path, mode='r') as rt_sessions:
                for line in rt_sessions:
                    if line.startswith(KOUMBIT_RT):
                        user_name = line.split(' ')[1]
        except Exception:
            pass

    if not user_name:
        user_name = input("RT Username: ")
        if not user_name:
            raise ValueError

    # Unfortunately, we can't reuse the credential cookie in ~/.rt_sessions so
    # we have to prompt for the password every time. But not storing
    # credentials is better in general.
    pw = getpass.getpass('Rt password: ')

    try:
        tracker = client.Client("v2", user_name, pw, KOUMBIT_RT)
    except RequestException as e:
        raise ConnectionError("Login to RT failed: {}".format(e))

    return tracker


if __name__ == '__main__':
    # Since we're testing things out show debugging info.
    logging.basicConfig(level=logging.DEBUG)

    krt = connect_to_rt()

    new_support_tickets = krt.ticket.search(
            "Queue='support' and Status='open'",
            fields=["Subject", "Creator"])

    print(
        [i for i in map(lambda x: [x['Subject'], x['Creator']],
                        new_support_tickets["items"])]
    )
