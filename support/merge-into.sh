#!/bin/bash
#
# This is just a simple helper for merging tickets. RT-CLI does it for us but
# it's annoying to remember how to use it.
#
# By default it will show some information before doing the merge and ask you
# to confirm. you can remove this interactive behaviour by using -f
#
function usage()
{
  echo "usage: $0 [-f] [-c] <Ticket-ID-to-merge-others-into> <RT_TICKET_ID>..." >&2
  echo >&2
  echo "Merge a number of tickets into one." >&2
  echo "The first ticket ID specified is the 'destination', e.g. other tickets will be merged into this one." >&2
  echo "Multiple ticket IDs can be specified" >&2
  echo >&2
  echo "If you'd like to close the target ticket (e.g. the one in which all others are merged into) after" >&2
  echo "performing the merger, use -c before any ticket ID." >&2
  echo >&2
  echo "By default, this script will as you to confirm the action after showing you ticket titles" >&2
  echo "To disable interactive confirmation a title listing, use -f before any ticket ID." >&2
  echo >&2

  exit 1
}

INTERACTIVE="true"
CLOSE="false"
while [ $# -gt 0 ] ; do
  case "$1" in
    -f) INTERACTIVE="false";;
    -c) CLOSE="true";;
    -*) usage;;
    *) break;; ## stop processing when we get a non-option
  esac
  shift
done

TARGET=$1
shift

RT_TICKET="$@"

if [ -z "$TARGET" ] || [ -z "$RT_TICKET" ]; then
  usage
fi

if [ $INTERACTIVE == "true" ]; then
  echo "All of the following tickets:"
  for i in $RT_TICKET; do
    rt show -f id,queue,status,subject "$i" | sed 's/^/  /'
    echo "  --------"
  done
  echo
  echo "Will be merged into:"
  rt show -f id,queue,status,subject "$TARGET" | sed 's/^/  /'
  echo
  if [ "$CLOSE" == "true" ]; then
    echo "After performing this merger, the ticket $TARGET will be closed."
    echo
  fi
  echo -n "Really perform this operation? [y/N] "
  read confirm
  confirm=$(echo "$confirm" | tr '[:upper:]' '[:lower:]')
  if [ "$confirm" != "y" ]; then
    exit 0
  fi
fi

for i in $RT_TICKET; do
  rt merge "$i" "$TARGET"
done

if [ "$CLOSE" == "true" ]; then
  rt res "$TARGET"
fi

