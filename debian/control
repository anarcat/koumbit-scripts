Source: koumbit-scripts
Section: admin
Priority: optional
Maintainer: Guillaume Est Une Palourde <gbeaulieu@koumbit.org>
Uploaders: Gabriel Filion <gabriel@koumbit.org>, Sébastien Grenier <sebas@koumbit.org>, Nizar Koubaa <nizar@koumbit.org>, Alexandru Petrosan <alexandru@koumbit.org>, Kienan Stewart <kienan@koumbit.org>
Build-Depends: debhelper (>= 9.0.0)
Standards-Version: 4.3.0
#Vcs-Git: git://git.debian.org/collab-maint/koumbit-scripts.git
#Vcs-Browser: http://git.debian.org/?p=collab-maint/koumbit-scripts.git;a=summary

Package: koumbit-scripts-alternc
Architecture: all
Depends: ${shlibs:Depends}, ${misc:Depends}, python, php-cli | php5-cli, perl
Recommends: dstat
Breaks: koumbit-scripts (<< 2.0)
Replaces: koumbit-scripts (<< 2.0)
Description: Alternc administration tools for koumbit servers
 Various scripts that help administration of koumbit servers.
 .
 This package installs scripts that are used with alternc. They complement the
 scripts that AtlernC already provides in order to do more tasks automatically.

Package: koumbit-scripts-aegir
Architecture: all
Depends: ${shlibs:Depends}, ${misc:Depends}, python, locate
Breaks: koumbit-scripts (<< 2.0)
Replaces: koumbit-scripts (<< 2.0)
Description: Aegir administration tools for koumbit servers
 Various scripts that help administration of koumbit servers.
 .
 This package installs scripts that are used with aegir. They are mostly
 helper-tools for things that are either not automated or somewhat outside of
 aegir's reach but peripheral to it.

Package: koumbit-scripts-backup
Architecture: all
Depends: ${shlibs:Depends}, ${misc:Depends}
Breaks: koumbit-scripts (<< 2.0)
Replaces: koumbit-scripts (<< 2.0)
Description: Backup administration tools for koumbit servers
 Various scripts that help administration of koumbit servers.
 .
 This package installs scripts that are used to manage backups. These scripts
 are fairly specific to how Koumbit manages backups.

Package: koumbit-scripts-mail
Architecture: all
Depends: ${shlibs:Depends}, ${misc:Depends}, python, perl
Breaks: koumbit-scripts (<< 2.0)
Replaces: koumbit-scripts (<< 2.0)
Description: Email administration tools for koumbit servers
 Various scripts that help administration of koumbit servers.
 .
 This package installs scripts that help maintenance on email servers. They
 mostly deal with parsing postfix logs to make it easier to find information in
 them.

Package: koumbit-scripts-misc
Architecture: all
Depends: ${shlibs:Depends}, ${misc:Depends}, python, php-cli | php5-cli, perl
Breaks: koumbit-scripts (<< 2.0)
Replaces: koumbit-scripts (<< 2.0)
Description: Random administration tools for koumbit servers
 Various scripts that help administration of koumbit servers.
 .
 This package installs scripts that perform general tasks or that didn't fit
 elsewhere.

Package: koumbit-scripts-monitoring
Architecture: all
Depends: ${shlibs:Depends}, ${misc:Depends}, perl
Breaks: koumbit-scripts (<< 2.0)
Replaces: koumbit-scripts (<< 2.0)
Description: Monitoring scripts for koumbit servers
 Various scripts that help administration of koumbit servers.
 .
 This package installs some monitoring scripts that Koumbit uses. These scripts
 are either nagios/icinga2 plugins, and also script for running an IRC bot that
 reports nagios status changes.

Package: koumbit-scripts-payscript
Architecture: all
Depends: ${shlibs:Depends}, ${misc:Depends}, perl
Breaks: koumbit-scripts (<< 2.0)
Replaces: koumbit-scripts (<< 2.0)
Description: Scripts for managing pay slips for Koumbit
 Various scripts that help with processing pay slips.
 .
 The scripts make it possible to encrypt pay slips with gpg if the public key
 is known and then to send the slips to the correct corresponding email address.

Package: koumbit-scripts-svn
Architecture: all
Depends: ${shlibs:Depends}, ${misc:Depends}
Breaks: koumbit-scripts (<< 2.0)
Replaces: koumbit-scripts (<< 2.0)
Description: Svn administration tools for koumbit servers
 Various scripts that help administration of koumbit servers.
 .
 This package installs scripts that help managing svn. Those scripts are mainly
 just boilerplate for creating repositories and fixing file permissions for
 them. They are probably very specific to how Koumbit used to configure SVN
 repositories on servers.

Package: koumbit-scripts-vps
Architecture: all
Depends: ${shlibs:Depends}, ${misc:Depends}
Breaks: koumbit-scripts (<< 2.0)
Replaces: koumbit-scripts (<< 2.0)
Description: Vps administration tools for koumbit servers
 Various scripts that help administration of koumbit servers.
 .
 This package installs scripts that help out with administration and
 maintenance tasks for VPSes.

Package: koumbit-scripts-wikifarm
Architecture: all
Depends: ${shlibs:Depends}, ${misc:Depends}, python
Breaks: koumbit-scripts (<< 2.0)
Replaces: koumbit-scripts (<< 2.0)
Description: Wikifarm administration tools for koumbit servers
 Various scripts that help administration of koumbit servers.
 .
 This package installs scripts that help out with maintaining a moinmoin
 wikifarm. They mostly automate what is documented about how to maintain a
 wikifarm.

Package: koumbit-scripts-support
Architecture: all
Depends: ${shlibs:Depends}, ${misc:Depends},
 rt4-clients,
 swaks
Recommends: python3-dnspython,
 python3-termcolor,
 python3-dnspython,
 python3-future
Description: Scripts to help with support tasks at koumbit
 Various scripts that help administrators deal with support issues.
 .
 This package installs scripts that help out with administration and
 maintenance tasks for administrators doing support at Koumbit. Most of them
 interact with RT to perform some cleanup tasks or send routine responses for
 legal requests and such.
