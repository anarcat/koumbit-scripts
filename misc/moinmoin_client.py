#!/usr/bin/env python3
"""Library for interacting with moinmoin's web pages.

This library is intended to be used by web users and wiki administrators
without SSH or local access to the moinmoin storage files and commands.

required pipy projects (in debian usually python3-$project):
    * requests
    * ratelimiter

This is meant to be a library that mechanizes some actions on pages and their
content.

moinmoin does not have a proper REST or whatever-HTTP API, so this library
needs to use the same GET and POST requests that a user would use on the
"interactive" web pages. This means that all actions that need to send out
forms to submit data need to use a valid CSRF token, so two requests are
needed to achieve the action. This procedure is handled automatically by this
library.

Through the requests library, this lib handles all of the url encoding, so all
values can just be handed over as-is and they "should" be able to reach
moinmoin as intended.
"""

from typing import Optional
import logging
import difflib

import requests
from urllib.parse import urljoin
from ratelimiter import RateLimiter


class MoinException(Exception):
    """An error occurred while interacting with moinmoin."""

    pass


class RateLimit:
    """Simple object that represents a rate limit.

    This is just a method of imposing the presence of both values.
    """

    def __init__(self, calls: int, seconds: int) -> None:
        """Validate values and set them as attributes."""
        if calls < 1:
            raise ValueError(
                "The number of calls in a rate limit should be a non-zero positive number."  # noqa:E501
            )
        if seconds < 1:
            raise ValueError(
                "Seconds in a rate limit should be a non-zero positive number."
            )

        self.calls = calls
        self.seconds = seconds


class Moin:
    """Generic actions on moinmoin pages."""

    def __init__(self, url: str, cookies: Optional[dict] = None,
                 get_rate_limit: Optional[RateLimit] = None,
                 post_rate_limit: Optional[RateLimit] = None) -> None:
        """Initialize internal state.

        Choosing a rate limit that won't get you blocked is a bit of an art..
        Check out your wiki configuration to see if the limits were changed.
        Furthermore, verify :file:`MoinMoin/config/multiconfig.py` which
        contains the default values. Make sure to also check the "all" limit
        which acts as a kind of "lower limit" (e.g. even though an action is
        set to a more permissive threshold, if you pass the "all" threshold
        you'll still trip moinmoin's rate limiting. There might also be some
        differences in counting time so if you use exactly the threshold
        that's configured in moinmoin you might still trip it. Usually, if you
        set your limit to the same number of calls but with seconds +1 you
        should be able to avoid tripping the rate limit. Finally, note that
        limits are set per action, so GET and POST requests might be working
        on different thresholds depending on what you do with the library.

        :param url: Base URL of the moinmoin instance.
        :param cookies: Inject cookies into the requests.Session object. By
          using this, you can reuse the authentication cookie that you got in
          another object or in a previous script run and if your token is valid
          you'll be able to avoid the need to call :meth:`login`.
        :param get_rate_limit: Impose a rate limit on the number of GET
          requests made per interval in seconds. If not set, no rate limit is
          imposed.
        :param post_rate_limit: Impose a rate limit on the number of GET
          requests made per interval in seconds. If not set, no rate limit is
          imposed.
        """
        self.url = url
        self.session = requests.Session()
        if cookies is not None:
            self.session.cookies.update(cookies)
        self.logger = logging.getLogger(__name__)

        if get_rate_limit is not None:
            get_rate_limiter = RateLimiter(max_calls=get_rate_limit.calls,
                                           period=get_rate_limit.seconds)
            self._get_rate_limiter = get_rate_limiter
        else:
            self._get_rate_limiter = None

        if post_rate_limit is not None:
            post_rate_limiter = RateLimiter(max_calls=post_rate_limit.calls,
                                            period=post_rate_limit.seconds)
            self._post_rate_limiter = post_rate_limiter
        else:
            self._post_rate_limiter = None

    def _get(self, *args, **kwargs) -> requests.Response:
        """Rate-limit GET requests, if needed.

        .. seealso:: :meth:`__get`
        """
        if self._get_rate_limiter is not None:
            with self._get_rate_limiter:
                res = self.__get(*args, **kwargs)
        else:
            res = self.__get(*args, **kwargs)

        return res

    def __get(self, path: str,
              params: Optional[dict] = None) -> requests.Response:
        """Send a GET request to self.url + path.

        :param path: URL to request. This can be an absolute URL or a path
          relative to the base url.
        :returns: Response received from the requests library.
        :raises: HTTPError if the request returned an error code.
        """
        full_url = urljoin(self.url, path)
        resp = self.session.get(full_url, params=params)
        resp.raise_for_status()
        return resp

    def _post(self, *args, **kwargs) -> requests.Response:
        """Rate-limit post requests, if needed.

        .. seealso:: :meth:`__post`
        """
        if self._post_rate_limiter is not None:
            with self._post_rate_limiter:
                res = self.__post(*args, **kwargs)
        else:
            res = self.__post(*args, **kwargs)

        return res

    def __post(self, path: str, data: dict) -> requests.Response:
        """Send a POST request with data as a payload.

        :param path: URL to request. This can be an absolute URL or a path
          relative to the base url.
        :param data: Dictionary of information to send as payload with the
          POST request.
        :returns: Response received from the requests library.
        :raises: HTTPError if the request returned an error code.
        """
        full_url = urljoin(self.url, path)
        resp = self.session.post(full_url, data=data)
        resp.raise_for_status()
        return resp

    def find_definition(self, text: str, term: str, max_length: int = 256) -> str:
        """Within text, find the the value of a defition by term name

        :param text: The string of text to search in.
        :param term: The term to search for
        :param max_length: The maximum length of the defition to serach for. If
        the end character ('\n') is not found before then, nothing matches.
        """
        return self._find_in_text(text, " {}:: ".format(term), '\n', max_length)

    def _find_in_text(self, text: str, pre_str: str, post_str: str,
                      post_max_offset: int) -> str:
        """Within text, find the characters between pre_str and post_str.

        :param text: The string of text to search in.
        :param pre_str: The sub-string that should be placed right before the
          characters that we're looking for.
        :param post_str: The sub-string that should be placed right after the
          characters that we're looking for.
        :param post_max_offset: Maximum number of characters to look for
          post_str starting from right after the position of pre_str. This
          value should only act as a safeguard to cut search times after it
          doesn't seem reasonable that we'll ever find post_str anymore. Thus
          this parameter should always be given a value that is meaningful
          according to the text that's being searched for.
        :returns: The string of characters placed between pre_str and post_str.
        :raises: MoinException if either pre_str or post_str is not found
          within :param:`text`.
        """
        self.logger.debug("Looking for pre_str: %s", pre_str)
        pre_index = text.find(pre_str)
        if pre_index == -1:
            raise MoinException(
                "Could not find prefix delimitor '{}' in text.".format(
                    pre_str))

        self.logger.debug("Found pre_str at index %s", pre_index)

        needle_start_index = pre_index + len(pre_str)

        end_offset = needle_start_index + post_max_offset
        self.logger.debug("Looking for post_str: %s", repr(post_str))
        post_index = text.find(post_str, needle_start_index, end_offset)
        if post_index == -1:
            raise MoinException(
                "Could not find postfix delemitor '{}' in text.".format(
                    post_str))

        self.logger.debug("Found post_str at index %s", post_index)

        return text[needle_start_index:post_index]

    def _obtain_ticket(self, page: str,
                       params: Optional[dict] = None) -> tuple:
        """Request a page and find the CSRF token in the response.

        moinmoin calls CSRF tokens "tickets".

        :param page: Path to the page. This can be an absolute URL or a path
          relative to the base URL. It should not contain URL parameters.
        :returns: A tuple (token, text) containing the string containing the
          token and the text of the get response. The text of the response can
          be used for gathering further information.
        :raises: MoinException if there's an error while grabbing the CSRF
          ticket.
        """
        editor_resp = self._get(page, params)
        ticket = self._find_in_text(
            editor_resp.text, '<input type="hidden" name="ticket" value="',
            '"', 80)
        self.logger.debug("Got ticket %s", ticket)

        return (ticket, editor_resp.text)

    def login(self, username: str, password: str) -> dict:
        """Login to moinmoin and grab a session cookie.

        :param username: User name used for login.
        :param password: Password used for login.

        :returns: A dictionary of cookies present after login. You can store
          the authentication cookie somewhere and reuse it by passing it in a
          dictionary to a new Moin instance.
        :raises: HTTPError if http request was unsuccessful.
        :raises: MoinException if login did not produce a session cookie even
          though POSTing to the login page returned a success code. Moinmoin
          doesn't use HTTP codes to signify login errors.
        """
        self.logger.debug("Logging in to %s", self.url)

        credentials = {
            # The first two are magic hidden fields on the login page and need
            # to be present otherwise you don't get a session cookie
            "action": "login",
            "login": "Login",
            "name": username,
            "password": password,
        }

        # Session will grab the session cookie when we get a response to this.
        self._post("action/login", data=credentials)
        if len(self.session.cookies) == 0:
            raise MoinException("Login could not grab a session cookie.")

        cookies = dict(self.session.cookies)
        return cookies

    def page_raw(self, page: str, revision: int = 0) -> str:
        """Grab raw content of :term:`page` and return it.

        :param page: Path to the page. This can be an absolute URL or a path
          relative to the base URL. It should not contain URL parameters
        :param revision: If set to a non-zero value, the page content that is
          returned will correspond to the content at this revision. Using 0 as
          the revision number will fetch the content for the current latest
          revision. A page in the deleted state will give a 404 error since
          sematically, "there is no current revision". Specific (non-zero)
          revision numbers of a deleted page can however still be used.
        :raises: ValueError if a negative value was passed to
          :param:`revision`.
        """
        self.logger.info("Requesting raw content of page %s", page)
        params = {"action": "raw"}
        if revision < 0:
            raise ValueError("A page revision number should not be negative")

        params.update({"rev": revision})

        resp = self._get(page, params=params)

        return resp.text

    def diff(self, page: str, src_revision: int, dst_revision: int) -> str:
        """Show unified diff between two revisions of a page.

        :param page: Path to the page. This can be an absolute URL or a path
          relative to the base URL. It should not contain URL parameters.
        :param src_revision: Revision number that represents the "origin" text
          in the diff. Negative values are not valid.
        :param dst_revision: Revision number that represents the "changed" text
          in the diff. Negative values are not valid.
        :returns: A string containing a unified diff between src_revision's
          and dst_revision's raw texts.
        :raises: ValueError if src_revision or dst_revision are passed
          negative values.
        """
        if src_revision < 0 or dst_revision < 0:
            raise ValueError(
                format(
                    "Revision numbers should not be negative: src {}; dst {}",
                    src_revision, dst_revision))

        # :func:`unified_diff` needs lists of lines
        src = self.page_raw(page, src_revision).split("\n")
        dst = self.page_raw(page, dst_revision).split("\n")

        diff_generator = difflib.unified_diff(
            src, dst, fromfile="{}.rev{}".format(page, src_revision),
            tofile="{}.rev{}".format(page, dst_revision), lineterm="")

        return "\n".join(list(diff_generator))

    def edit_page(self, page: str, new_content: str,
                  comment: str = "") -> None:
        """Set the content of a page.

        :param page: Path to the page. This can be an absolute URL or a path
          relative to the base URL. It should not contain URL parameters.
        :param new_content: Then entire new text that the page will contain.
        :param comment: Description for the new revision that we're creating
          for the page.
        :raises: HTTPError in case a request fails. Make sure to check the
          error code, a 404 code could mean that the page doesn't exist yet.
        :raises: MoinException if there's an error while grabbing the CSRF
          ticket or the current revision.
        """
        self.logger.info("Modifying page %s", page)

        ticket, resp_text = self._obtain_ticket(
                page, params={"action": "edit", "editor": "text"})

        current_rev = self._find_in_text(
            resp_text, '<input type="hidden" name="rev" value="', '"',
            15)
        self.logger.debug("Got rev %s", current_rev)

        edit_payload = {
            # These two magic values need to be present for the edit to work
            "action": "edit",
            "editor": "text",
            "ticket": ticket,
            "rev": current_rev,
            "savetext": new_content,
            "comment": comment,
        }

        self._post(page, data=edit_payload)

    def rename_page(self, page: str, new_page_name: str, comment: str = "",
                    rename_subpages: bool = False,
                    rename_redirect: bool = False) -> None:
        """Rename a page to a different name.

        When a page is renamed, it brings along all of its revisions. So to
        moinmoin it is like the old page name has never existed.

        :param page: Path to the page. This can be an absolute URL or a path
          relative to the base URL. It should not contain URL parameters.
        :param new_page_name: New nage that the page will be taking. This is
          not a URL, so it snould only contain the page name.
        :param comment: Description for the new revision that we're creating
          for the page: deleting creates a deleted version of the page.
        :param delete_subpages: A flag that determines if subpages should be
          deleted recursively.
        :param rename_redirect: A flag that enables creation of a new page in
          the old name with a content that will redirect visitors to the new
          page name. This will only take effect if the `show_rename_redirect`
          wiki configuration option is enabled, and will otherwise be silently
          ignored by moinmoin.
        :raises: MoinException if there's an error while grabbing the CSRF
          ticket.
        """
        self.logger.info("Renaming page %s to %s", page, new_page_name)

        ticket, resp_text = self._obtain_ticket(
                page, params={"action": "RenamePage"})

        rename_payload = {
            "action": "RenamePage",
            # The "trigger button" needs to be present for magic to happen
            "rename": "Rename",
            "ticket": ticket,
            "comment": comment,
            "newpagename": new_page_name,
        }
        if rename_subpages:
            rename_payload.update({"rename_subpages": "1"})
        if rename_redirect:
            rename_payload.update({"rename_redirect": "1"})

        # BUG: if renaming back and forth with the same name, moinmoin will
        # start outputting an error in the HTML output about the page already
        # existing (even though nothing is visible in action=info) but this
        # lib will only see a success because of the 200 return code. It would
        # be way better to spot those errors and raise an exeption but it
        # might mean that we'll have to somwhat parse the HTML to figure this
        # out :S
        return self._post(page, data=rename_payload)

        # TODO: if rename_redirect: check that the old page exists, otherwise
        # raise an exception

    def delete_page(self, page: str, comment: str = "",
                    delete_subpages: bool = False) -> None:
        """Delete a page from the wiki.

        A deleted page will still be present in the wiki as a series of
        revisions, but it won't be visible in any search result.

        :param page: Path to the page. This can be an absolute URL or a path
          relative to the base URL. It should not contain URL parameters.
        :param comment: Description for the new revision that we're creating
          for the page: deleting creates a deleted version of the page.
        :param delete_subpages: A flag that determines if subpages should be
          deleted recursively.
        :raises: MoinException if there's an error while grabbing the CSRF
          ticket.
        """
        self.logger.info("Deleting page %s", page)

        ticket, resp_text = self._obtain_ticket(
                page, params={"action": "DeletePage"})

        delete_payload = {
            "action": "DeletePage",
            # The "trigger button" needs to be present for magic to happen
            "delete": "Delete",
            "ticket": ticket,
            "comment": comment,
        }
        if delete_subpages:
            delete_payload.update({"delete_subpages": "1"})

        try:
            self._post(page, data=delete_payload)
        except requests.HTTPError as e:
            if e.response.status_code == 404:
                # Since we've just deleted the page, moinmoin returns a 404 to
                # indicate that deletion did happen correctly.
                pass
            else:
                raise
