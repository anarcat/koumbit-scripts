#!/usr/bin/php -q
<?php

header("Content-type: text/plain");

# where to make tests
$dir = "images";
# if we want to remove the files after creation
$cleanup = true;

# no user-serviceable parts below
$file = "$dir/test-".date(DATE_ATOM)."-".sha1(openssl_random_pseudo_bytes(10));
print "test d'ecriture dans le fichier: $file\n";

$made_dir = $false;

print "directory exists? ";

if (file_exists($dir)) {
  if (is_dir($dir)) {
    print "yes\n";
  }
  else {
    exit("error: $dir is not a directory");
  }
}
else {
  print "no";
  if (mkdir($dir)) {
    $made_dir = true;
    print ", created\n";
  }
  else {
    exit(", error: cannot create directory $dir");
  }
}

$ret = is_writable($dir);
print "directory is_writable? " . var_export($ret, TRUE) . "\n";

print "file exists? ";
if (file_exists($file)) {
  exit("not overwriting $file");
}
else {
  print "no\n";
}
$ret = is_writable($file);
print "file is_writable? " . var_export($ret, TRUE) . "\n";

$fpc = file_put_contents($file, "test\n");
print "file_put_contents(): " . var_export($fpc, TRUE) . "\n";
print "file_get_contents(): " . file_get_contents($file);

if (!$cleanup) {
  exit("not removing files");
}

if ($fpc !== FALSE) {
  $ret = unlink($file);
  print "removing file $file: " . var_export($ret, TRUE) . "\n";
}
if ($made_dir) {
  $ret = rmdir($dir);
  print "removed dir $dir: " . var_export($ret, TRUE) . "\n";
}
