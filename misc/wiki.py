#!/usr/bin/python3
"""CLI interface to remotely interact with Koumbit's moinmoin.

This is a CLI interface that uses the moinmoin_client library to provide some
useful actions by interacting with a moinmoin wiki.

This module can also be used as a library for interacting with the Koumbit
wiki. The rate limiting configured for the moinmoin client should provide a
certain amount of speed of execution without getting clients banned, and they
are tuned up for Koumbit's moinmoin instance configuration.
"""

import os
import getpass

import fire

import moinmoin_client


class WikiCLI:
    """Main CLI interface."""

    URL = "https://wiki.koumbit.net/"

    def __init__(self):
        """Create attributes."""
        self.__moin = None

    def _login(self) -> moinmoin_client.Moin:
        """Authenticate with the moinmoin website."""
        # It's unclear for now if this tool will be used for running massive
        # actions on the wiki -- but it might! -- so we'll set rate limits that
        # seem to be working on our wiki to avoid tripping the rate limit for
        # the "raw" action or for the "all" counter and getting blocked by
        # moinmoin.
        get_limit = moinmoin_client.RateLimit(25, 6)
        post_limit = moinmoin_client.RateLimit(80, 60)
        self.__moin = moinmoin_client.Moin(
            self.URL, get_rate_limit=get_limit,
            post_rate_limit=post_limit)

        user = os.environ.get("MOIN_USER")
        if user is None:
            user = input("wiki user: ")
        pw = os.environ.get("MOIN_PW")
        if pw is None:
            pw = getpass.getpass("wiki pass: ")

        self.__moin.login(user, pw)

        return self.__moin

    def _moin(self) -> moinmoin_client.Moin:
        """Return the moin client interface. Establish connection if needed."""
        if self.__moin is None:
            return self._login()
        return self.__moin

    def get_raw_page(self, page_name: str):
        """Grab the raw contents of a page and display it.

        :param page_name: URL of a page to obtain. It can be a full URL,
          provided that it points to the same base URL that was used for login,
          or it can be just the name of the page that you would concatenate to
          the base URL in order to access it (e.g. `SomePath/SomePage`).
        """
        moin = self._moin()

        return moin.page_raw(page_name)

    def rename_page(self, orig_page: str, dest_page: str) -> None:
        """Rename one page.

        :param orig_page: Current page name.
        :param dest_page: The name that the page will be renamed to.
        """
        moin = self._moin()

        moin.rename_page(orig_page, dest_page)


if __name__ == "__main__":
    wiki = WikiCLI()
    fire.Fire(wiki)
