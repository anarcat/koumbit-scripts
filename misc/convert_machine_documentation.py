#!/usr/bin/env python3
"""
Script to convert a documentation from the monolith format to
the newer Corps/Esprit/TrouDeRack format

Install dependencies:
  sudo apt install python3-dotenv python3-fire

Example run:
  convert_machine_documentation.py convert_page example.koumbit.net 128-N --platform X11-128
"""

import logging
logging.basicConfig()
logging.getLogger().setLevel(logging.DEBUG)
import os
import pathlib

import dotenv
import moinmoin_client as mmc
import fire

class DocumentationConverter(object):
    r"""
    Convert machine documentation from the monolithic format
    to Corps/Esprit/TrouDeRack format.
    """
    SlotInfo = {
        'slot': {
            'terms': ['Slot'],
            'token': '%%SLOT%%',
        },
        'rack': {
            'terms': ['Rack', 'Location'],
            'token': '%%RACK%%',
        },
        'switch': {
            'terms': ['Switch'],
            'token': '%%SWITCH%%',
        },
        'switch port': {
            'terms': ['SwitchPort', 'Switch Port'],
            'token': '%%SWITCHPORT%%',
        },
        'switch 2': {
            'terms': ['Switch2'],
            'token': '%%SWITCH2%%',
        },
        'switch port': {
            'terms': ['Switch2Port', 'Switch2 Port'],
            'token': '%%SWITCH2PORT%%',
        },
        'console': {
            'terms': ['Console'],
            'token': '%%CONSOLE%%',
        },
        'console port': {
            'terms': ['Console port'],
            'token': '%%CONSOLEPORT%%',
        },
        'console baud': {
            'terms': ['Console baud', 'Baud'],
            'token': '%%BAUD%%',
        },
        'pdu': {
            'terms': ['PDU'],
            'token': '%%PDU%%',
        },
        'pdu port': {
            'terms': ['PDU port'],
            'token': '%%PDUPORT%%',
        },
    }
    BodyInfo = {
        'name': {
            'terms': ['Name', 'Nom'],
            'token': '%%NOM%%',
        },
        'platform': {
            'terms': ['Plateforme'],
            'token': '%%PLATEFORME%%',
        },
        'racked on': {
            'terms': ['Racké', 'Activation'],
            'token': '%%RACKE%%',
        },
        'spirit': {
            'terms': ['Esprit'],
            'token': '%%ESPRIT%%',
        },
        'slot': {
            # This is a compound link [[%%RACK%%/%%SLOT%%]] usually
            'compound': True,
            'join': '/',
            'terms': ['Location', 'Slot'],
            'token': '%%SLOT%%',
            'link': True,
        },
        'rail type': {
            'terms': ['Rail'],
            'token': '%%RAIL%%',
        },
        'odoo reference': {
            'terms': [],
            'token': '%%ODOOREF%%',
        },
        'depth': {
            'terms': ['Depth'],
            'token': '%%DEPTH%%',
        },
        'cost': {
            'terms': ['Cost'],
            'token': '%%COST%%',
        },
        'photo': {
            'terms': ['Photo'],
            'token': '%%PHOTO%%',
        },
        'hotswap': {
            # This could be a platform thing, but sometimes the chassis may vary
            'terms': ['Hotswap access', 'Hotswap'],
            'token': '%%HOTSWAP%%',
        },
        'drive bays': {
            'terms': ['Drive bays'],
            'token': '%%DRIVENAYS%%',
        },
        'mac interface 1': {
            'terms': ['mac1', 'MAC1'],
            'token': '%%MAC1%%',
        },
        'mac interface 2': {
            'terms': ['mac2', 'MAC2'],
            'token': '%%MAC2%%',
        },
        'motherboard serial number': {
            'terms': ['Board S/N'],
            'token': '%%BOARDSN%%',
        },
        # 'hardware bugs': {
        #     'terms': [],
        #     'token': None
        # },
    }
    SpiritInfo = {
        'name': {
            'terms': ['Name', 'Nom'],
            'token': '%%NAME%%',
        },
        'ip': {
            'terms': ['IP'],
            'token': '%%IP%%',
        },
        'customer': {
            'terms': ['Customer'],
            'token': '%%CUSTOMER%%',
        },
        'cluster': {
            'terms': ['Cluster'],
            'token': '%%CLUSTER%%',
        },
        'monitoring': {
            'terms': ['Monitoring'],
            'token': '%%MONITORED%%',
        },
        'service': {
            'terms': ['Service'],
            'token': '%%SERVICE%%',
        },
        'os': {
            'terms': ['OS'],
            'token': '%%OS%%',
        },
        'configuration management': {
            'terms': ['Configuration management'],
            'token': '%%CONFIGMANAGED%%',
        },
        'cluster': {
            'terms': ['Cluster'],
            'token': '%%CLUSTER%%',
        },
        'hard drives': {
            'terms': [],
            'token': '%%SDDDEFS%%',
        },
        'ssh host key': {
            'terms': ['SshHostKey'],
            'token': '%%SSHHOSTKEY%%',
        },
    }

    def __init__(self, base_url: str, user: str, password: str):
        self.base_url = base_url
        self.body_template_url = "{}/CorpsTemplate".format(base_url)
        self.spirit_template_url = "{}/EspritsTemplate".format(base_url)
        self.slot_template_url = "{}/TrousDeRackTemplate".format(base_url)
        self.logger = logging.getLogger(__name__)
        self.client = mmc.Moin(base_url)
        self.client.cookies = self.client.login(user, password)

    def convert_page(self, page: str, body_name: str, spirit_name: str = None, platform = None, noop = False):
        """Convert an old documentation to te new one

        :param page: The page to convert
        :param body_name: The name of the new body page
        :param spirit_name: The name of the new spirit page
        :param platform: The name of the platform reference for the body. The name will
          be converted to a wiki ling
        """
        if spirit_name is None:
            spirit_name = page
        if platform:
            platform = "[[Plateformes/{}]]".format(platform)
        page_url = "{}/{}".format(self.base_url, page)
        body_url = "{}/Corps/{}".format(self.base_url, body_name);
        spirit_url = "{}/Esprits/{}".format(self.base_url, spirit_name);
        #slot_url = "{}/{}/{}".format(self.base_url, cabinet, slot)
        # Does the page we want exist?
        page_body = self.client.page_raw(page)
        if not page_body:
            self.logger.error("Source page '{} does not exist or has no content".format(page))
            return False
        # Build information from original
        body, spirit, slot = self._get_templates();
        spirit_wiki_link = "[[Esprits/{}]]".format(spirit_name)
        slot = self._fill_template(slot, page_body, self.SlotInfo, {'spirit': spirit_wiki_link})
        body = self._fill_template(
            body, page_body, self.BodyInfo,
            {'name': body_name, 'platform': platform, 'spirit': spirit_wiki_link})
        disk_refs = self._build_hard_drive_string(page_body)
        spirit = self._fill_template(spirit, page_body, self.SpiritInfo,
                                     {'name': spirit_name, 'hard drives': disk_refs})

        # Save the body, spirit and slot
        slot_info = self.BodyInfo['slot']
        slot_info['link'] = False
        slot_page = self._find_term(page_body, 'slot', slot_info)
        # Occasionally we get half links instead, just pull out all the square brackets.
        slot_page = slot_page.replace('[', '').replace(']', '')

        comment = "convert_machine_documentation.py from page {}".format(page)
        try:
            if noop:
                self.logger.info("Esprits/{} page -----:\n{}".format(spirit_name, spirit))
                self.logger.info("Corps/{} page   -----:\n{}".format(body_name, body))
                self.logger.info("{} page         -----:\n{}".format(slot_page, slot))
            else:
                self.client.edit_page("Esprits/{}".format(spirit_name), spirit, comment)
                self.logger.info("Saved page Esprits/{}".format(spirit_name))
                self.client.edit_page("Corps/{}".format(body_name), body, comment)
                self.logger.info("Saved page Corps/{}".format(body_name))
                self.client.edit_page(slot_page, slot)
                self.logger.info("Saved page {}".format(slot_page))
        except Exception as e:
            self.logger.error("Failed to save one of the pages: {}".format(e))
            return False

        # Add redirection in original page to spirit
        if noop:
            self.logger.info("Would add redirection to 'Esprits/{}'".format(spirit_name))
        else:
            try:
                page_body = "#REDIRECT Esprits/{}\n".format(spirit_name)
                self.client.edit_page(page, page_body)
                self.logger.info("Saved page {}".format(page))
            except Exception as e:
                self.logger.error("Failed to save page '{}': {}".format(page, e))
                return False
        return True

    def _build_hard_drive_string(self, source: str) -> str:
        char = 'a'
        end_char = 'z'
        sddref = ''
        failed = 0
        while char < end_char:
            if failed > 3:
                break
            terms = ["Sd{}Model".format(char), "Sd{}Sn".format(char), "Sd{}FwRevision".format(char)]
            values = ['N/A', 'N/A', 'N/A']
            for index, term in enumerate(terms):
                value = 'N/A'
                try:
                    value = self.client.find_definition(source, term)
                except:
                    pass
                values[index] = value
            # If we don't have a serial number, consider it failed
            if values[1] == 'N/A':
                failed += 1
                char = chr(ord(char) + 1)
                continue
            for index, term in enumerate(terms):
                sddref += " {}:: {}\n".format(term, values[index])
            char = chr(ord(char) + 1)
        return sddref

    def _find_term(self, source: str, key, info, override = dict()) -> str:
        replacement_value = ''
        v = None
        compound = True if ('compound' in info.keys() and info['compound']) else False
        join = info['join'] if ('join' in info.keys()) else ''
        link = info['link'] if ('link' in info.keys()) else False
        self.logger.debug("{}: {}".format(key, info['terms']))
        for t in info['terms']:
            self.logger.debug("Searching source for definition '{}'".format(t))
            try:
                v = self.client.find_definition(source, t)
            except mmc.MoinException:
                continue
            if v:
                replacement_value += v.strip() + join
            if not compound:
                break
        if compound:
            replacement_value = replacement_value.rstrip(join)
        if link:
            replacement_value = "[[{}]]".format(
                replacement_value.replace('[', '').replace(']', '')
            )
        if key in override.keys():
            self.logger.debug("Overriding '{}' with '{}' for '{}'".format(
                replacement_value, override[key], key))
            replacement_value = override[key]
        return replacement_value

    def _fill_template(self, template: str, source: str, info, override = dict()) -> str:
        """
        Returns the template string with tokens filled based on the source and template information structure
        """
        for key, value in info.items():
            replacement_value = self._find_term(source, key, value, override)
            if not replacement_value:
                self.logger.debug("No value for terms associated with '{}' found".format(key))
                if 'default' in value.keys():
                    self.logger.debug("Using default value '{}' for '{}'".format(
                        value['default'], key))
                    replacement_value
                else:
                    continue
            self.logger.debug("Replacing token '{}' with '{}'".format(value['token'], replacement_value))
            template = template.replace(value['token'], replacement_value)
        return template

    def _get_templates(self):
        body_template = self.client.page_raw(self.body_template_url)
        if not body_template:
            self.logger.error("Unable to retrieve page source for body template '{}'".format(self.body_template))
            raise ValueError
        spirit_template = self.client.page_raw(self.spirit_template_url)
        if not spirit_template:
            self.logger.error("Unable to retrieve page source for spirit template '{}'".format(self.spirit_template_url))
            raise ValueError
        slot_template = self.client.page_raw(self.slot_template_url)
        if not slot_template:
            self.logger.error("Unable to retrieve page source for slot template '{}'".format(self.slot_template_url))
            raise valueError
        return (body_template, spirit_template, slot_template)

if __name__ == '__main__':
    env_path = pathlib.Path.home() / pathlib.Path('.config/koumbit/wiki')
    dotenv.load_dotenv(dotenv_path=env_path, verbose=True)
    url = os.environ.get('WIKI_URL', '')
    user = os.environ.get('WIKI_USER', '')
    password = os.environ.get('WIKI_PASSWORD', '')
    dc = DocumentationConverter(url, user, password)
    fire.Fire(dc)
