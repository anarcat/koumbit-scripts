#! /usr/bin/python

# this script increments the "version" (in the form N(-M.x)), but just
# the last chunk
# exmaple:
# 1.1 -> 1.2
# 1 -> 2
# 1.1.2-3 -> 1.1.2-4

import re
import sys

version = sys.argv[1]
print "version: %s" % version
groups = re.match(r'([0-9]+)(([-.][0-9]+)*)?([-.][0-9]+)|([0-9]+)', version).groups()
print groups
if groups[-1]:
    versioninc = str(int(groups[-1]) + 1)
else:
    last = groups[-2]
    print "last chunk: %s" % last
    g = re.match(r'([-.])([0-9]+)', last).groups()
    print g
    print "the digit: %s" % g[1]
    lastinc = int(g[1]) + 1
    print "incremented: %d" % lastinc
    versioninc = groups[0] + groups[1] + g[0] + str(lastinc)
print 'back together: %s' % versioninc
