#!/bin/sh

# If we have an existing page of that name, we simply update the field.
# If we don't, well, we crash. 

usage() {
  cat <<EOF
This thing calls WikiEdit to set a field. Its not validating any
inputs.

You need to specify those four things ; stuff is hackish for now

usage: $0 -p PageName => modify PageName
       $0 -f 'field' field to be filled
       $0 -v 'value' value to be associated to the field above
EOF
  exit
}

args=`getopt hn:t:f:v: $*`
BASEDIR=$(dirname "$0")

set -- $args

for i
do
  case "$i"
  in
     -n)
          NAME=$2
          numArg=$((numArg+1))
          shift 2;;
     -f)
          FIELDS=$2
          echo $FIELDS
          numArg=$((numArg+1))
          shift 2;;
     -v)
          VALUES=$2
          numArg=$((numArg+1))
          shift 2;;
     -h)
          usage;
          exit;;
     --)
          shift; break;;
  esac
done

RUN=
COUNT=0

if ${BASEDIR}/wiki-edit.sh -p "$NAME" -G > /tmp/patate3321 ;  
then
  for I in $(echo $FIELDS | tr % ' ');
  do
    COUNT=$(expr $COUNT + 1)
    if cat /tmp/patate3321 | grep " ${I}::" ; 
    then
      if cat /tmp/patate3321 | grep " ${I}:: $(echo $VALUES | cut -d%  -f$COUNT)" > /dev/null ; 
      then 
        SED="sed \"s/ / /\""
      else
        # OKay, we just need to update the field
        SED="sed \"/ ${I}::/c \\\ ${I}:: $(echo $VALUES | cut -d%  -f$COUNT)\""
      fi
    else
      SED="sed \"i \ ${I}:: $(echo $VALUES | cut -d%  -f$COUNT)\""
    fi
    if [ "$RUN" = "" ] ;
    then
      RUN="$SED";
    else
      RUN="$RUN | $SED";
    fi
  done
fi

cat /tmp/patate3321 | eval $RUN > /tmp/patate3322

DIFF=$(diff /tmp/patate3321 /tmp/patate3322)

if [ "$DIFF" != "" ] 
then
  cat /tmp/patate3321 | eval $RUN | ${BASEDIR}/wiki-edit.sh -p $NAME -S
  exit $?
else
  exit 1;
fi
