#!/usr/bin/env python3
"""

Prereqs: apt install python3-pands

To get the csv:

  1. log in to icinga
  2. go to the "Recent call notifications" menu entry
  3. expand the filter options (by default it is less than 1 day old)
  4. Change the timestamp filter option to the desired age (eg -30 days for the last month)
  5. apply (be patient!)
  6. in the dropdown arrow of the top-menu (next  to the notifications title),
     select CSV and save the file

Starting off manually:

  import notification_analysis as na
  a = na.read('/home/x/Downloads/notifications.csv')


Some things that might be useful to get out of the data:

1. Plot graph of notifications over time

   # This is a bit weird, but I haven't found a better way yet.
   a.resample('1s', on='timestamp').sum().plot(y='count')
   na.plt.show()

2. What is the frequency of calls across the hours of a day for the period?
  * we find how many calls there were per period (eg. hour) across the given range (total calls in hour)
  * divided by the number of days in the period gives us average calls per hour of the day

  a['hour'] = a['timestamp'].dt.hour
  # percentage of the sum of occurences grouped by hour
  a.groupby(['hour']).sum().div(len(a)).mul(100).plot(y='count', kind='bar')
  na.plt.show()

  # or by dayt
  a['day'] = a['timestamp'].dt.dayofweek
  a.groupby(['day']).sum().div(len(a)).mul(100).plot(y='count', kind='bar')
  na.plt.show()

3. Frequency/heat table across week + timeperiod
  * for each hour of each day of week we find how many notifications were sent

  fig, axes = na.plt.subplots(nrows = 1, ncols = 7)
  na.plt.subplots_adjust(wspace=0.2, hspace=0.5)
  for i in range(0, 7):
    a[a.day == i].groupby(['hour']).sum().div(len(a)).mul(100).plot(ax=axes[i], y='count', kind='bar')
  na.plt.show()

4. filter by service or host or type

  a[a.service == 'something']

  # or

  a[a.host == 'something']

5. which hosts had the most notifications

  a.groupby('host').sum().sort_values(['count'], ascending = False).head(15)

6. which services had the most notifications

  a[a['type'] == 'service'].groupby('service').sum().sort_values(['count'], ascending = False).head(15)

7. par type

  a.groupby('type').sum().sort_values(['count'], ascending = False).head(10)

"""
import csv

import numpy
import pandas
import matplotlib.pyplot as plt

class IcingaEvent:

    def __init__(self, host, service, timestamp, state, _type = "service"):
        self.host = host
        self.service = service
        self.timestamp = timestamp
        self.state = state
        self._type = _type

def read_icinga_csv (filename):
    """
    Returns a panda dataframe with host, service, timestamp, state, type (host
    or service), and count (an integer - 1 by default).
    The count is used as a column we can sum against.
    """
    with open(filename) as csvfile:
        reader = csv.DictReader(csvfile);
        return pandas.DataFrame(
            [{'host': row['host_name'], 'service': row['service_description'],
             'timestamp': pandas.to_datetime(row['notification_timestamp'], unit='s'),
              'state': row['notification_state'],
              'type': 'service' if row['service_description'] != '' else 'host',
              'count': 1}
             for row in reader if row['notification_timestamp'] != ''],
            columns = ['host', 'service', 'timestamp', 'state', 'type', 'count'])
