# Installation

On stretch/buster:

```
sudo apt install python3-spur python3-yaml python3-pip
sudo pip3 install pyCLI
```

# Configuration

Create a servers.yaml which describes the servers and location you want to
search for Drupal installations.

# Usage

Run an output configuration for mass patching to platforms.yaml:

```
./find_drupal_installations -c servers.yaml -o platforms.yaml
```

