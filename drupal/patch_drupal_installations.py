#!/usr/bin/env python3

import cli.log
import io
import spur
import yaml

@cli.log.LoggingApp
def patch_drupal_installations(app):
    with io.open(app.params.config_file, 'r') as f:
        app.log.info('Loading configuration from {}'.
                     format(app.params.config_file))
        c = yaml.load(f)
        for server in c:
            s = server.split('@')
            user = s[0]
            host = s[1]
            if app.params.hosts is not None and host not in app.params.hosts:
                app.log.debug(
                    'Skipping host {} since not white-listed in command-line'.
                             format(host)
                )
                continue
            # Open ssh connection
            app.log.info('Attempting to connect to {} with user {}'.
                    format(host, user))
            shell = spur.SshShell(hostname = host, username = user)
            with shell:
                result = shell.run(['mktemp'], allow_error = True)
                tmp_file = result.output.decode().strip('\n\r ')
                if not tmp_file:
                    app.log.warn('Unable to create tmp_file: {}'.format(
                        result.stderr_output.decode()))
                else:
                    app.log.info('Created tmp_file: {}'.format(
                        tmp_file))
                result = shell.run(['wget', app.params.patch, '-O',
                                    tmp_file], allow_error = True)
                if result.return_code != 0:
                    shell.run(['rm', tmp_file], allow_error = True)
                    app.log.warn('Unable to download patch {} to {}'.
                                 format(app.params.patch, tmp_file))
                    continue
                app.log.info('Downloaded {} to {} on {}'.format(
                    app.params.patch, tmp_file, server))
                for drupal in c[server]:
                    base_directory = drupal['directory']
                    version = drupal['version']
                    if app.params.drupal_version and not version.startswith(app.params.drupal_version):
                        app.log.info(
                            'Skipping {}:{} since version {} does not match {}'.
                            format(server, base_directory,
                                   version, app.params.drupal_version)
                        )
                        continue
                    patch_command = ['patch', '-b', '-p1']
                    if app.params.noop:
                        patch_command.extend(['--dry-run'])
                    patch_command.extend(['-i', tmp_file])
                    result = shell.run(patch_command,
                                       cwd = base_directory, allow_error = True)
                    if result.return_code != 0:
                        app.log.warn('Failed to patch {}:{}'.
                                     format(server, base_directory))
                        app.log.warn('(stdout) output: ' + result.output.decode())
                        app.log.warn('(stderr) output: ' + result.stderr_output.decode())
                    else:
                        noop_text = ''
                        app.log.debug('Patch output: {}'.format(result.output.decode()))
                        if app.params.noop:
                            noop_text = '[noop] '
                        app.log.info('{}Patched {}:{}'.
                                     format(noop_text, server, base_directory))
                shell.run(['rm', tmp_file], allow_error = True)

patch_drupal_installations.add_param(
    '-c', '--config', default = 'platforms.yaml', action = 'store',
    dest = 'config_file', help = 'Configuration file for the patching. Default: platforms.yaml'
)
patch_drupal_installations.add_param(
    '-d', '--hostname', action = 'append', dest = 'hosts'
)
patch_drupal_installations.add_param(
    '-n', '--noop', action = 'store_true', default = False, dest = 'noop'
)
patch_drupal_installations.add_param(
    'patch',
    help = 'The URL of the patch. It should be downloaded via wget/curl',
)
patch_drupal_installations.add_param(
    '--drupal-version', action = 'store', dest = 'drupal_version'
)

if __name__ == '__main__':
    patch_drupal_installations.run()
