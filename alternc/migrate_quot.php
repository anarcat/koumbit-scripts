#! /usr/bin/php -q
<?

require_once('/var/alternc/bureau/class/config_nochk.php');

$q = $db->query('SELECT MAX(uid) FROM membres;');

if ($db->next_record($q)) {
  $max = $db->Record[0];
}

$DATA_PART = '/dev/sda6'; # might not be necessary for parsing
$NEW_PART = '/dev/hdb2';

for ($user = 2000; $user <= $max; $user++) {
  $quot = split("\n", `quota -g $user`);
  preg_match("!$DATA_PART\s+\d+\s+(\d+)!", $quot[2], $matches);
  $size = $matches[1];

  system("/usr/sbin/setquota -g $user $size $size 0 0 $NEW_PART");
}

?>