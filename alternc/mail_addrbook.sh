#! /bin/sh

# we assume there is a $domain subdirectory in $path and it contains
# directories named as usernames, and those directory in turn contain a
# $addr_file file
#
# in other words:
# temp/
#   alternatives.ca/
#     $user/
#        Maildir/
#          sqwebmail-addressbook

# XXX: currently, this doesn't send any email. the "> $user" should probably be
# replaced by something like "| sendmail", but should be extensively tested with
# a single email first

from='"Support technique" <support@koumbit.net>'
addr_file=Maildir/sqwebmail-addressbook

# nothing to configure below this line

path=$1; shift
domaine=$1; shift

usage () {

cat <<EOF
$0 path domaine user ...
Will send the user's address book to user@$domaine, with a nice friendly message.

We assume $path exists and is properly setup, see the source code for details.
EOF
}

if [ $# -lt 1 ]
then
  usage
  exit
fi

for user in $*; do 
   addr_book=$path/$domaine/$user/$addr_file
   if [ -r $addr_book ]; then 
     ( 
     cat <<EOF
From: $from
To: $user@$domaine
Mime-version: 1.0
Content-type: text/plain; charset=iso-8859-1
Content-Transfer-Encoding: 8bit
Subject: votre carnet d'adresses

Bonjour!

Voici le contenu de votre carnet d'addresses, import� de l'ancien webmail.

L'�quipe technique de Koumbit,

--
EOF

# '
     cat $addr_book
     ) > $user #| sendmail $user@$domaine && echo $user
   fi
done
