#! /bin/sh

mkdir -p /var/alternc/bind/offzones
for zone in $* ; do
  gesdns=`mysql --defaults-file=/etc/alternc/my.cnf -e "select gesdns from domaines where domaine = '$zone' and gesdns <> 0;" -B -N`
  if [ -z "$gesdns" ]; then
    echo $zone is down, moving to offzones
    mv /var/alternc/bind/zones/$zone /var/alternc/bind/offzones/
  fi
done
