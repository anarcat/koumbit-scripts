#!/bin/sh
# Apache check process script
# This script was written by Carl Pickering
# modified by anarcat for simplicity
# Install using /etc/crontab

# for some reason, this script doesn't find stuff in /usr/sbin without this
PATH=/usr/sbin:/usr/bin:/sbin:/bin
export PATH

LOCKFILE="/var/run/apache_process_check.lock"

THRESHOLD=${1:-70}

if [ -e "${LOCKFILE}" ]; then
    if ps $(cat ${LOCKFILE}) > /dev/null ; then
        logger -i -p daemon.notice -t $0 -- "lockfile ${LOCKFILE} already in use, aborting"
        exit 1
    else
        logger -i -p daemon.notice -t $0 -- "stale lockfile ${LOCKFILE} detected (pid $(cat ${LOCKFILE}) not found), removing"
        rm -f "${LOCKFILE}"
    fi
fi
# write pid to lockfile
echo $$ > "${LOCKFILE}"

APACHE_PROCS=$(ps faux | grep apache2 | grep -v "grep")
NUMHTTPD=$(printf "%s" "$APACHE_PROCS" | wc -l)
logger -i -p daemon.debug -t $0 -- "${NUMHTTPD} apache2 threads found"

if [ ${NUMHTTPD} -gt ${THRESHOLD} ]; then
    logger -i -s -p daemon.warn -t $0 -- "Too many apache2 threads (${NUMHTTPD} > ${THRESHOLD}), restarting"
    printf "%s" "$APACHE_PROCS"
    apache2ctl fullstatus &
    # stop apache, but don't fail over stopping failures
    service apache2 stop | logger -i -p daemon.debug -t $0 -s || true
    # check if stop failed
    if pidof apache2; then
        # give it time to recover
        sleep 5
        # make sure not processes are left
        killall -9 apache2 | logger -i -p daemon.debug -t $0 -s
    fi
    # start it again in any case
    service apache2 start | logger -i -p daemon.debug -t $0 -s
elif [ ${NUMHTTPD} -le 0 ]; then
    logger -i -s -p daemon.warn -t $0 -- "Apache seems to be stopped (${NUMHTTPD} processes found), restarting"
    service apache2 start | logger -i -p daemon.debug -t $0 -s
fi
# remove lockfile in any case
rm -f "${LOCKFILE}"
