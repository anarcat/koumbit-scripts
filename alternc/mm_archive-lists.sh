#!/bin/sh -x

# Petit script utile pour archiver des listes mailman en batch.
# Pourrait etre repris pour archiver des listes sur le bureau alternc

# Emplacement des archives.
ARCHIVES="/var/alternc/archives/listes/"

# Fichier qui liste les listes a archiver.
LISTE_DE_LISTES=`cat /var/alternc/archives/liste-de-listes.txt `

# Variables mailman
MM_ARCHIVES="/var/lib/mailman/archives/private/"
MM_CONFIG="/var/lib/mailman/lists/"

for LISTE in $LISTE_DE_LISTES; do

REPERTOIRE=$ARCHIVES$LISTE 
  if [[ ! -d $REPERTOIRE ]]; then
    mkdir -p $REPERTOIRE
  fi

# archivage des abonnes de la liste 
        /usr/sbin/list_members -o "$REPERTOIRE"/abonnes.txt "$LISTE"

# archivage des archives de la liste
	/bin/cp -rp $MM_ARCHIVES$LISTE.mbox $REPERTOIRE

# archivage de la config de la liste
	/bin/cp -rp $MM_CONFIG$LISTE/config.pck $REPERTOIRE

done
