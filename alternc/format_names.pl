#! /usr/bin/perl -w

use strict;
if ($#ARGV < 0) {
    print <<EOF
Usage: $0 <domain>
  where domain is the domain of the users.
EOF
;
    die("not enough arguments\n");
}
my $domain = $ARGV[0];

while (<STDIN>) {
    # split the line on whitespace
    my ($user, $pass) = split;
    # check for wrong format
    if (!defined($user) || !defined($pass)) {
        die("parse error");
    }
    print $domain, " ", $user, " 1 ", $pass, "\n";
}
