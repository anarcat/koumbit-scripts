#!/bin/sh

# Count the number of spip installation

ALTERNCDIR=/var/alternc/html/
if [ -n "$1" ] ; then
    ALTERNCDIR="$1"
fi
find $ALTERNCDIR -name inc_version.php* -type f ! -path \*.svn\* | while read file
do
	# spip > 2.0
	spipversion=`grep '^\$spip_version_branche = "' $file | awk '{print $3}' | sed "s/'//g" | sed "s/\"//g" | sed "s/;//"`
	if [ $spipversion ] ; then 
		echo "$file $spipversion" 

	# spip < 2.0
	else 
	spipversion=`grep '^\$spip_version_affichee = ' $file | awk '{print $3}' | sed "s/'//g" | sed "s/\"//g" | sed "s/;//"` 
	echo "$file $spipversion" ;

	fi

done
