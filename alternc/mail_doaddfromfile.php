#! /usr/bin/php
<?php
/*
 $Id$
 ----------------------------------------------------------------------
 AlternC - Web Hosting System
 Copyright (C) 2002 by the AlternC Development Team.
 http://alternc.org/
 ----------------------------------------------------------------------
 Based on:
 Valentin Lacambre's web hosting softwares: http://altern.org/
 ----------------------------------------------------------------------
 LICENSE

 This program is free software; you can redistribute it and/or
 modify it under the terms of the GNU General Public License (GPL)
 as published by the Free Software Foundation; either version 2
 of the License, or (at your option) any later version.

 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.

 To read the license please visit http://www.gnu.org/copyleft/gpl.html
 ----------------------------------------------------------------------
 Original Author of file: Benjamin Sonntag
 Purpose of file: Create a new mail account
 ----------------------------------------------------------------------
*/

require_once("../class/config.php");

  /* ----------------------------------------------------------------- */
  /** The usr_list is the file that has the list of mails to be created.
   * It must have the following format in order to work correctly.
   * To create mailboxes:
   * domain left_side_of_the_mail 1 (1 if pop) password
   * here is a example:
   * ----------------------------
   * alterc.org tech 1 goalternc 
   * ----------------------------
   * To create alias:
   * domain left_side_of_the_mail 0 (for alias) * (to skip the password) redirection 
   * ----------------------------
   * alterc.org techs 0 * tech@alternc.org
   * ----------------------------
   * NB: there can only be one redirection per alias.
   */

$fileinfo = fopen("/var/alternc/import-data/usr_list", "r");


# Read data from fileinfo into all the variables
while ($data = fscanf($fileinfo, "%s\t%s\t%d\t%s\t%s\n")) {

require_once("/var/alternc/bureau/class/config_nochk.php");
list ($domain, $email, $pop, $pass, $alias) = $data;

#now print for viewing:
echo ("Domain: $domain\n");
echo ("Mail: $email\n");

    if ($pop=="1"){
	echo ("Password: ****\n");
	echo ("Type : mailbox\n");
    }
    else if ($pop=="0") {
	echo ("Type: alias to $alias\n");
    }
    else {
	echo ("<p class=\"error\">ERROR : check your source file.</p>");
    }

if (!$mail->add_mail($domain,$email,$pop,$pass,$alias)) {
	$error=$err->errstr();
        $addok=0;
	echo "<blockquote><p class=\"error\">$error</p></blockquote>";
} 

else {
	$addok=1;
        echo "<blockquote>The mailbox $email@$domain has been successfully created </blockquote>";
}

}
fclose($fileinfo);
$error=0;
require_once("mail_list.php");

exit();
?>
