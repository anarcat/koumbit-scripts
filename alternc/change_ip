#! /bin/sh

record_regexp='\(.*[[:space:]]\+IN[[:space:]]\+A[[:space:]]\+\)'

usage() {
    cat <<EOF
Usage: $0 <oldip> <newip> <zonefile> ...

Change the <oldip> string to <newip>.

WARNING: <oldip> is fed directly to sed as a regular expression, and
will be replaced everywhere. It will be however, preceded by the
following regexp to match only IN records:

${record_regexp}

EOF
}

if [ $# -lt 3 ]; then
    usage
    exit
fi

OLDIP=$1; shift
NEWIP=$1; shift
TMP=${PWD}

# external command dependencies
awk=awk
mv=mv
cut=cut
sed=sed
echo=echo
date=date
printf=printf
mktemp=mktemp

# increment a serial number in a zone file
#
# we assume that the serial line contains the "serial string", eg.:
#                 2005012703      ; serial
#
# from AlternC do_domaines.sh
#
# returns 1 if file isn't readable
# returns 2 if we can't find the serial number
# returns 3 if a tempfile can't be created
increment_serial() {
    if [ -f "$1" ]; then
        # the assumption is here
        SERIAL=`$awk '/^..*serial/ {print $1}' < $1` || return 2
        if [ ! -z "${SERIAL}" ]; then
            DATE=`$echo $SERIAL | $cut -c1-8`
            ORDRE=`$echo $SERIAL | $sed s/"${DATE}0\?"/""/g`
            DATE_JOUR=`$date +%Y%m%d`
            # increment the serial number only if the date hasn't changed
            if [ "X$DATE" = "X$DATE_JOUR" ] ; then
                ORDRE=$(($ORDRE+1))
            else
                ORDRE=1
                DATE=$DATE_JOUR
            fi
            NEW_SERIAL=$DATE`$printf "%.2d" $ORDRE`
            TMPFILE=`$mktemp $1.XXXXXX` || return 3
            # put the serial number in place
            $awk -v NEW_SERIAL=$NEW_SERIAL '{if ($3 =="serial") print "		"NEW_SERIAL "	; serial"; else print $0}' < $1 > $TMPFILE && \
                $mv -f $TMPFILE $1
            return 0
        else
            return 2
        fi
    else
        return 1
    fi
}

#
# change the IP of a DNS record
#
# returns 1 if we can't find the record to change
# returns 3 if we can't create a tempfile
#
change_ip () {
    # escape potential wildcards (dots, usual in IP addresses..)
    oldip=`echo $1 | sed -e 's/\\./\\\./g'`
    newip=$2
    TMPFILE=`$mktemp $3.XXXXXX` || return 3
    match=`$sed -n '/'${record_regexp}${oldip}'/p' < $3`
    if [ -z "$match" ]; then
        rm -f $TMPFILE
        return 1
    else
        $sed -e 's/'${record_regexp}${oldip}'/\1'${newip}'/' < $3 > $TMPFILE && \
            mv -f $TMPFILE $3 || rm -f $TMPFILE
        return 0
    fi
}

# do everything
for i; do
    echo -n "changing ${OLDIP} to ${NEWIP} in ${i}: " >&2
    if [ -s ${i} ]; then
        change_ip ${OLDIP} ${NEWIP} ${i}
        if [ $? -lt 1 ]; then
            increment_serial ${i}
            echo "ok" >&2
        else 
            if [ $? = 1 ]; then
                echo "not found" >&2
            fi
        fi
    else
        echo "ignored" >&2
    fi
done