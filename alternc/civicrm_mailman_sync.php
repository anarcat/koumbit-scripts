#!/usr/bin/php
<?php

# [ML] 2008-10-19 Script qui synchronise une liste de membres CiviCRM avec une liste Mailman
# c.f.: http://wiki.civicrm.org/confluence/display/CRMDOC/Mailman+synchronization

require_once '/var/alternc/html/f/facilguls/facil.qc.ca/drupal/sites/default/civicrm.settings.php';
require_once 'CRM/Core/Config.php';
require_once 'api/v2/Contact.php';

$config =& CRM_Core_Config::singleton();

global $tmpfilename;
global $mmparams;


// Add the Mailman sync_members parameters here
// see http://www.tin.org/bin/man.cgi?section=8&topic=sync_members for syntax
// -w=no : do not send welcome message
// -g=no : do not send byebye message
// -a=yes : inform administrator of new subscriber
$mmparams = "-n -w=no -g=no -a=yes";

function SyncList($mmlistname, $GroupId) {
 # [ML] global $tmpfilename;
  global $mmparams;

  // Open the temporary file
  $tmpfilename = '/tmp/sync2mailman-facil.txt'; // Temporary file location
  $f = fopen($tmpfilename, 'w+');

  // Set search criteria
  $params['rowCount'] = 50000;
  $params['sort'] = 'sort_name ASC';
  $params['return.display_name'] = 1;
  $params['return.email'] = 1;
  $groups = array($GroupId => 1);
  $params['group'] = $groups;
  $result = civicrm_contact_search( $params );

  // Process the search results
  $nrows = 0;  
  foreach ($result as $id => $values) {
    if ($values['email'] != '') {
      $fix_displayname = str_replace('  ', ' ', $values['display_name']); // remove extra spaces in some names
      $line = trim($fix_displayname) . ' <' . trim($values['email']) . '>' . "\n";
      fwrite($f, $line);
      $nrows++;
    }
  }

  if ($nrows > 0) { // Zero rows is probably an error
    // Run the mailman sync_members command
    $command = "/usr/sbin/sync_members $mmparams -f $tmpfilename $mmlistname 2>&1";
    echo "Running $command\n";
 #[ML]   $output =  shell_exec($command);
 #[ML] do not delete members for now
 $output = shell_exec("/var/lib/mailman/bin/add_members -r $tmpfilename -w n -a y $mmlistname  2>&1");
    echo $output;
  }
  else
    echo "WARNING no rows... skipping sync command.\n";

  // Close the temporary file
  fclose($f);
  // unlink($tmpfilename);
}

SyncList('annonces-facil.qc.ca','3'); // specify your mailing list name and CiviCRM group id here
SyncList('membres-facil.qc.ca','3'); // specify your mailing list name and CiviCRM group id here
// repeat for each list
