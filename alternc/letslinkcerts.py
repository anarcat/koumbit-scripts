#!/usr/bin/env python3

import argparse
import logging
import os
import os.path
import re

logger = logging.getLogger()
logger.setLevel(logging.INFO)

def linkletsencrypt(directory, noop = False):
    if not os.path.exists(directory):
        logging.error('Directory "{}" does not exist'.format(directory))
        return -1
    link_to_candidates = {}
    # List all the directories we have
    pattern = re.compile("(?P<fqdn>[-\.\w]+)-(?P<number>\d+)")
    for entry in os.scandir(directory):
        if not os.path.isdir(entry.path):
            continue
        logging.debug('Found directory "{}"'.format(entry.path))
        match = pattern.match(entry.name)
        if not match:
            logging.debug('Directory "{}" does not match fqdn-NNNN, assuming it is okay'.format(entry.name))
            continue
        if match.group('fqdn') not in link_to_candidates:
            link_to_candidates[match.group('fqdn')] = {}
        link_to_candidates[match.group('fqdn')][int(match.group('number'))] = entry.name
    for base_name, candidates in link_to_candidates.items():
        logging.debug('Checking candidates for basename "{}": {} candidates found'.format(base_name, len(candidates)))
        path = os.path.join(directory, base_name)
        if os.path.exists(path):
            exists = True
            is_link = os.path.islink(path)
            t = 'N/A'
            if is_link:
                t = os.path.realpath(path)
            logging.debug('Cert "{}": Base path "{}" exists (is link: "{}", target: "{}")'.format(
                base_name, path, is_link, t))
        else:
            exists = False
            t = 'N/A'
            is_link = False
            logging.debug('Cert "{}": base path "{}" does not exist'.format(base_name, path))
        logging.debug('Cert "{}" candidates: {}'.format(base_name, candidates))
        best_target = '{}-{:04}'.format(base_name, max(candidates.keys()))
        logging.debug('Cert "{}": Best target: "{}"'.format(base_name, best_target))
        if not exists:
            if noop:
                logging.info('New symlink: Would link "{}" -> "{}"'.format(base_name, best_target))
            else:
                # base_name will point to best_target
                os.symlink(os.path.join(directory, best_target),
                           os.path.join(directory, base_name))
                logging.info('Linked "{}" -> "{}"'.format(base_name, best_target))
        else:
            if is_link:
                if os.path.join(directory, best_target) == t:
                    if noop:
                        logging.info('Do nothing: Link "{}" -> "{}" is already best match'.format(
                            base_name, best_target))
                    else:
                        logging.info('Skipping "{}", already at best match'.format(base_name))
                else:
                    if noop:
                        logging.info('Change symlink: Would change link "{}" -> "{}" (was: "{}")'.format(
                            base_name, best_target, t))
                    else:
                        os.remove(os.path.join(directory, base_name))
                        os.symlink(os.path.join(directory, best_target),
                                   os.path.join(directory, base_name))
                        logging.info('Changed link "{}" to "{}" (from "{}")'.format(
                            base_name, best_target, t))
            else:
                if noop:
                    logging.warning('Panic: "{}" exists and is not a link, but "{}" is a better match'.format(base_name, best_target))
                else:
                    logging.warning('Skipping "{}": base exists but is not a link, and a better match "{}" exists'.format(base_name, best_target))


if __name__ == '__main__':
    parser = argparse.ArgumentParser()
    parser.add_argument('-d', '--directory', help = 'Which directory run on',
            default = '/etc/letsencrypt/live')
    parser.add_argument('-n', '--noop', action = 'store_true',
            help = "Don't actually make any changes", default = False)
    parser.add_argument('-v', '--verbose', action = 'store_true',
            help = 'Output more information', default = False)
    args = parser.parse_args()
    if args.verbose:
        logger.setLevel(logging.DEBUG)
    exit(linkletsencrypt(args.directory, args.noop))


