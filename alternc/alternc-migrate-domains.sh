#!/bin/bash
##########################################################################
# Generic Migration Script for SQL databases in alternc for user's domaine
##########################################################################
MYSQLCNF="/etc/alternc/my.cnf"

function usage {
	echo "Usage:   $0 login [remote compte id]" >&2
	echo "" >&2
	echo "Example: $0 bobby 3141" >&2
	echo "" >&2
	echo "$0 takes one or two arguments:" >&2
	echo "" >&2
	echo "1- the login used in alternc to find all the domains" >&2
	echo "attached to it and generate a dump ready to be restored." >&2
	echo "2- the remote id in the 'compte' field to which we'll assign the domains" >&2
	echo "" >&2
	echo "Edit $MYSQLCNF for initial user, pass and database." >&2
}

# parse commandline
args=`getopt lnh $*`
set -- $args

# 0 is "ACTION_INSERT"
action=0
for i
do      
        case "$i" in
              -n) shift; dryrun=1;;
              -h) shift; usage; exit;;
              -l) shift; action=1;; # ACTION_UPDATE
              --) shift; break;;
        esac
done

if [ $# -lt 1 ]; then
	usage
	exit 1
fi

##################################
# FIXME: Add checks to see if DB can connect, etc...

###############################################################
# check the user id
LOGIN="$(echo $1 | tr -cd '[:alnum:]')"
USERID=$( mysql --defaults-file=$MYSQLCNF -B -e \
"select uid from membres where login='$LOGIN';" | tail -1 )

if [ $# -lt 2 ]; then
	REMOTE_USER="$USERID"
else
	REMOTE_USER="$2"
fi
REMOTE_USER="$(echo $REMOTE_USER | tr -cd '[:digit:]')"

echo "$1 has UID = '$USERID' which will be replaced by '$REMOTE_USER'" >&2

###################################################################
# dump the domain table
# 
# Get dump, remplace local ID by remote ID
if [ "$action" -lt 1 ]; then
  mysqldump --defaults-file=$MYSQLCNF --no-create-info --no-create-db -c --extended-insert=0 --where="compte='$USERID'" system domaines |\
   sed "s/VALUES\ (${USERID},/VALUES\ (${REMOTE_USER},/"
fi

echo ""
echo "-- trigger host creation in cronjob"
echo "INSERT INTO domaines_standby SELECT compte,domaine,mx,gesdns,gesmx,${action} as action from domaines where compte=${REMOTE_USER};"

####################################################################
### dump subdomain table
####################################################################
if [ "$action" -lt 1 ]; then
  echo ""
  echo "-- subdomain configuration"
  echo ""
  mysqldump --defaults-file=$MYSQLCNF --no-create-info --no-create-db  -c --extended-insert=0  --where="compte='$USERID'" system sub_domaines |\
   sed "s/VALUES\ (${USERID},/VALUES\ (${REMOTE_USER},/"
fi

echo ""
echo "-- trigger subdomain in cronjob"
echo "INSERT INTO sub_domaines_standby SELECT compte,domaine,sub,valeur,type,$action as action from sub_domaines where compte=${REMOTE_USER};"

####################################################################
### email dumps
####################################################################

# XXX: this only does forwards
mysqldump --defaults-file=$MYSQLCNF --no-create-info --no-create-db -c --extended-insert=0  --where="uid='$USERID'" system mail_domain |\
 sed "s/,${USERID},/,${REMOTE_USER},/"


