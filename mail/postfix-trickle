#! /bin/sh -e

limit=${1:-200}
release=${2:-1}
sleep=${3:-20}

usage() {
    cat <<EOF
Usage: $0 [ limit [ release [ sleep ] ] ]

Release <release> messages until <limit> messages are found in the
Postfix deferred queue, sleep $sleep seconds between.

Typical usage, in a panic:

    # put all emails on hold
    postsuper -h ALL
    # release one email every 20 seconds up to 10 emails in defered queue
    postfix-trickle 10
    # in a separate window, look at the queue
    watch -n1 -c "postqueue -j | grep -v hold | jq -C .recipients[]"
EOF
}

count() {
    queue=$1; shift
    find "/var/spool/postfix/$queue/" -type f | wc -l
}

hold=$(count hold)
while test $hold != 0; do
    deferred=$(count deferred)
    if [ $deferred -lt "$limit" ]; then
        echo "$hold held, $deferred deferred"
        mailq | grep '^[A-F0-9]' | awk '{print $1}' | sed 's/!//' | tail -"$release" | postsuper -H -
        echo "flushing deferred queue"
        sendmail -q
    fi
    sleep "$sleep"
    hold=$(count hold)
done
