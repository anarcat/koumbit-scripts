#! /usr/bin/perl -w

use strict;

my $blacklist = "/etc/apache/blacklist.conf";
my @spammers = ();

open BLACKLIST, "<$blacklist" || die "can't find blacklist $blacklist\n";
while (<BLACKLIST>) {
  if (/^[1-9]/) {
    my @line = split(/\t/, $_);
    push (@spammers, $line[0]);
  } elsif (!/^$/ and !/^#/) {
    warn("parse error: $_ does not start with an IP, is not blank or a comment, please fix");
  }
}
close BLACKLIST;

foreach my $conffile (@ARGV) {

open CONF, "<$conffile" || die "can't open configuration file $conffile\n";
my @config = <CONF>; # slurp the file in memory:
my $config = join('', @config);
close CONF;
# generate an instance of the template
my $tmpl = <<EOF
##BEGIN TEMPLATE
## This part of the configuration is automatically generated from the blacklist in $blacklist. Edit it there and then run $0 again
      Order Allow,Deny
      Allow from all
      Deny from @spammers
##END TEMPLATE
EOF
;
# place it in the configuration file
$config =~ s/^##BEGIN TEMPLATE\n.*?^##END TEMPLATE\n/$tmpl/smg;
open(CONF, ">$conffile") || die "can't open configuration file $conffile\n";
print CONF $config;
close CONF;

}
