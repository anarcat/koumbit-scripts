#! /usr/bin/python
#
# Copyright (C) 2017 by Gabriel Filion
#
# This program is free software; you can redistribute it and/or
# modify it under the terms of the GNU General Public License
# as published by the Free Software Foundation; either version 3
# of the License, or (at your option) any later version.
# 
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
# 
# You should have received a copy of the GNU General Public License
# along with this program; if not, write to the Free Software 
# Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA.

"""Force a list to replace From: header to the list address

Lists that do not replace the From: header are hurting a server's IP reputation
by sending emails from some providers that disallow others to do so. For
example hotmail and yahoo use DMARC to disallow anyone else from sending mail
from their domain name. By replacing the From: header to the list address, we
avoid this situation and comply with email policy since the sender will then be
in a domain that's (hopefully) controlled by the mailing list operators.

This script is intended to be run as a bin/withlist script, i.e.

% bin/withlist -l -r force_munge_from listname [options]

Options:
    -v / --verbose
        Print what the script is doing.

If run standalone, it prints this help text and exits.

In order to run this script with "withlist", it needs to be installed in
/usr/lib/mailman/bin
"""

import sys
import getopt

import paths
from Mailman import mm_cfg
from Mailman.i18n import _


def usage(code, msg=''):
    print _(__doc__.replace('%', '%%'))
    if msg:
        print msg
    sys.exit(code)


def force_munge_from(mlist, *args):
    try:
        opts, args = getopt.getopt(args, 'v', ['verbose'])
    except getopt.error, msg:
        usage(1, msg)

    verbose = 0
    for opt, arg in opts:
        if opt in ('-v', '--verbose'):
            verbose = 1

    # Make sure list is locked.
    if not mlist.Locked():
        if verbose:
            print _('Locking list')
        mlist.Lock()

    # Munge From is the desired state. Its value is 1 in the from_is_list parameter.
    if mlist.from_is_list != 1:
        if verbose:
            print _('Value of from_is_list setting is wrong, changing it to "Munge From"')
        mlist.from_is_list = 1

    if verbose:
        print _('Saving list')
    mlist.Save()
    mlist.Unlock()


if __name__ == '__main__':
    usage(0)
