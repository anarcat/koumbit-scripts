#! /usr/bin/python
#
# Copyright (C) 2018 by Gabriel Filion
#
# This program is free software; you can redistribute it and/or
# modify it under the terms of the GNU General Public License
# as published by the Free Software Foundation; either version 3
# of the License, or (at your option) any later version.
# 
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
# 
# You should have received a copy of the GNU General Public License
# along with this program; if not, write to the Free Software 
# Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA.

"""Show or change the list of regular expressions of emails that are banned from subscription.

By default, list self-subscription is open to the public, so as soon as someone
knows a list's name, they can ask to subscribe. It is sometimes necessary to
ban some emails from registering to lists, especially when this is used for
spamming.

This script is intended to be run as a bin/withlist script, i.e.

% bin/withlist -l -r manage_ban_list listname [options]

Options:
    -c / --change
	Change the value of the mailing list's ban_list configuration. This
        value should be a multi-line string that contains one regular expression per
        line (so you might want to use \n to split lines.

If you use this script without the -c/--change option, then it prints out the
value of ban_list for the specified list and does not change anything.

If run standalone (e.g. as a script, so without withlist), it prints this help
text and exits.
"""

import sys
import getopt

from Mailman.i18n import _


def usage(code, msg=''):
    print _(__doc__.replace('%', '%%'))
    if msg:
        print msg
    sys.exit(code)


def manage_ban_list(mlist, *args):
    try:
        opts, args = getopt.getopt(args, 'c:', ['change='])
    except getopt.error, msg:
        usage(1, msg)

    new_value = ''
    apply_new_value = False
    for opt, arg in opts:
        if opt in ('-c', '--change'):
            apply_new_value = True
            new_value = arg

    if not apply_new_value:
        print mlist.ban_list
    else:
        # TODO: uncomment and test whenever we need to actually use it.
        print "would have changed list's ban_list\n from: %s\n to: %s" % (mlist.ban_list, new_value)
        #mlist.Lock()
        #mlist.ban_list = new_value
        #mlist.Save()
        #mlist.Unlock()


if __name__ == '__main__':
    usage(0)
