#!/bin/sh

BACKUP=www.foundationfuturegenerations.org
DUMPDIR=/srv/backups/custom-backup-$BACKUP
FILEFILE=$BACKUP
DBFILE=$BACKUP.sql
if [[ ! -d $DUMPDIR ]] ;
then
  mkdir -p $DUMPDIR ; 
fi

# we need 2 differents things here:
#  - all files
#  - databases


PLATFORM=`dirname $(dirname $(aegircd www.foundationfuturegenerations.org))`
if [[ -f $DUMPDIR/$FILEFILE.tar.gz ]] ; 
then 
  rm $DUMPDIR/$FILEFILE.tar.gz
fi

tar -hcf $DUMPDIR/$FILEFILE.tar $PLATFORM --exclude=$PLATFORM/sites

# we took the platform without the sites, now we take all the modules and all the 
# actual site
tar --append -h -f $DUMPDIR/$FILEFILE.tar $PLATFORM/sites/all
tar --append -h -f $DUMPDIR/$FILEFILE.tar $PLATFORM/sites/$BACKUP
gzip $DUMPDIR/$FILEFILE.tar

# From what I understand the only way of getting weeklies/monthlies/yearlies is to use 
# maildir in backupninja, so we want to compress everything
if [[ -f $DUMPDIR/$DBFILE.gz ]] ; 
then 
  rm $DUMPDIR/$DBFILE.gz
fi
su aegir -c "drush @$BACKUP sql-dump" > $DUMPDIR/$DBFILE
gzip $DUMPDIR/$DBFILE

