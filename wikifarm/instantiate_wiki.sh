#! /bin/sh -e

# externalize this:
WIKILOCATION=/usr/local/www/wiki
INSTANCE=${1:-testwiki}

USER=www
GROUP=www

PREFIX=/usr/local
SHARE=$PREFIX/share/moin

cd $WIKILOCATION
mkdir $INSTANCE                   # make a directory for this instance

# data, underlay and config
cp -R $SHARE/data $INSTANCE       # copy template data directory
cp -R $SHARE/underlay $INSTANCE   # copy underlay data directory
cp $SHARE/config/wikiconfig.py $INSTANCE   # copy wiki configuration sample file
ln -s $WIKILOCATION/$INSTANCE/wikiconfig.py $SHARE/config/$INSTANCE.py

# remove the empty theme dir to create a symlink to the real thing
rm $INSTANCE/data/plugin/theme/__init__.py*
rmdir $INSTANCE/data/plugin/theme
ln -s $SHARE/htdocs $INSTANCE/data/plugin/theme # the symlink can be remove when we need custom themes

# cgi config
#cp $SHARE/server/* $INSTANCE

# Perms
chown -R ${USER}:${GROUP} $INSTANCE
chmod -R u+rwX $INSTANCE
# the manual recommends that, but we ignore it
#chmod -R ug+rwX $INSTANCE         # USER.GROUP may read and write
chmod -R o-rwx $INSTANCE          # everybody else is rejected

#chown root $INSTANCE/moin.cgi $INSTANCE/*.py

echo $WIKILOCATION >> /var/log/installed_wikis.log
