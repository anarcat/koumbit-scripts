#!/usr/bin/python
# -*- coding: iso-8859-1 -*-
"""
    Moin upgrade wrapper for wikifarms

    @copyright: 2006 by The Anarcat <anarcat@anarcat.ath.cx>
    @license: GNU GPL, see COPYING for details.
"""

from MoinMoin.script.moin import run
from re import sub

import optparse, sys
parser = optparse.OptionParser()

parser.add_option(
    "--config-dir", metavar="DIR", dest="config_dir",
    help=("Path to the directory containing the wiki "
          "configuration files. [default: current directory]")
)

parser.parse_args(sys.argv)

sys.path.insert(0, '/export/wiki/config')

from farmconfig import wikis
sys.argv[2:2] = ["--wiki-url="]
for wiki in wikis:
    name = wiki[0]
    url = wiki[1]
    # XXX, hack: transform the regexp into a canonical url
    # we need canonical urls in the config for this to be clean
    # look for (foo|bar) and replace with foo
    url = sub('\(([^\|]*)(\|[^\)]*\))+', '\\1', url)
    # remove common regexp patterns and slap a protocol to make this a real url
    url = sub('[\^\$]|(\.\*)', '', url)

    mod = getattr(__import__(name), 'Config')
    print "*** calling moin for wiki %s (%s)" % (getattr(mod, 'sitename'), url)

    sys.argv[2:3] = ["--wiki-url=%s" % url]
    run()
