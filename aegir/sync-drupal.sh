#! /bin/sh

# mass git/cvs synchronisation
#
# this script is designed to be ran from a cronjob: it will not produce any
# output unless there's an error in the import

GITROOT=/srv/git/drupal

status=0
while read path ; do
	if [ -z "$path" ]; then
		echo path cannot be empty
		continue
	fi
	project=`echo $path | sed 's#^.*/##'`
	if [ -d $GITROOT/$path ]; then
		cd $GITROOT/$path
		output=`git-cvsimport -d :pserver:anonymous@cvs.drupal.org:/cvs/drupal contributions/$path -a -u -m -p -Z,9 2>&1`
	else
		cd $GITROOT
		output=`git-cvsimport -d :pserver:anonymous@cvs.drupal.org:/cvs/drupal -C $path contributions/$path -a -u -m -p -Z,9 2>&1`
	fi
	if [ $? -ne 0 ] ; then 
		echo sync of project $project failed, output: $output
		status=$?
	fi
done <<EOF
modules/decisions
modules/dynamic_persistent_menu
modules/update_status_aggregator
modules/og_read_only
modules/bandwidth
modules/livestream
modules/videocomment
modules/provision
modules/hosting
modules/drush
themes/eldir
profiles/hostmaster
EOF

# sync the drupal core too, but that's already done by someone else
cd $GITROOT/core 
output=`git pull 2>&1`
if [ $? -ne 0 ] ; then
	echo sync of drupal core failed, output: $output
	status=$?
fi
exit $status
