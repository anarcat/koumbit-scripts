#!/bin/sh

# THIS ONE IS STILL SKETCHY, use at your own risk. I haven't got it to test right yet

# This should be part of aegir

domain="$1"

usage() {
  echo Takes one argument which is the targetted domain
}

full_path=$(aegircd "$domain")
if [ $? == 1 -o $? == 2 ] ; then
  echo could not find one and only one platform for $domain , specify it as a third argument...
  echo eg: run find /var/aegir/platforms/ -iname \"$domain\" for yourself
fi
# todo: if we have more than one platform, we should copy both ?

# pretty ugly but works
platform=$(dirname $(dirname "$full_path"))

for i in $(find "$platform" -maxdepth 1 -type l -print) ; do 
  FILE=$(readlink $i) ; 
  cp -fr "$FILE" "${i}-temporaire" ;          # we first copy in the same directory so there is only a rename to do so the site get down too long
  chown -R aegir:www-data "${i}-temporaire" ; # we chown, once again before
  unlink "$i" ;                               # then unlink 
  mv "${i}-temporaire" "$i" ;                 # then rename
  rm -fr "$FILE" ;                            # then we remove the old stuff
done
