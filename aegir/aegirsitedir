#!/bin/bash
#
# To get some debugging info set envt var DEBUG to some non-null/non-zero value.
#
# This script requires the "locate" command to function correctly. However we
# don't want to add it to the dependencies of the koumbit-scripts package since
# we don't want locate to be installed on some machines. So it needs to be
# manually installed
#

function usage() {
  echoerr "error: no domain name given."
  echoerr "aegircd [-r|--aegir-root ROOT_PATH] domain_name"
  echoerr "    default ROOT_PATH: /var/aegir/platforms"
  exit 255
}

function echoerr() {
  echo "$@" 1>&2;
}

function echodebug() {
  if [ -n "$DEBUG" ]; then
    echo "debug: $@"
  fi
}

AEGIR_ROOT=/var/aegir/platforms

POSITIONAL=()
while [[ $# -gt 0 ]];
do
  key="$1"
  case "$key" in
    '-r'|'--aegir-root')
      AEGIR_ROOT="$2"
      shift
      shift
      ;;
  *)
    POSITIONAL+=("$1")
    shift
    ;;
  esac
done

FQDN="${POSITIONAL[0]}"
if [ -z $FQDN ]; then
  usage
fi

if ! (type -t locate >/dev/null); then
  echo "Command 'locate' could not be found. Make sure to install locate package in order to use this script." >&2
  exit 1
fi

if [ ! -d "$AEGIR_ROOT" ] ; then
    echo "Aegir root ($AEGIR_ROOT) does not exist"
    exit 1
fi

# in some cases, sites with the same domain name but with a prefix (e.g.
# ninja.com and fooninja.com) might confuse the locate. To ensure that we're
# matching $FQDN on a directory boundary -- and thus eliminate double matches
# -- we're using a / at the start of the locate pattern.
SITEDIR=$(locate "$AEGIR_ROOT*/$FQDN/drushrc.php" | sed 's,/drushrc.php,,')
echodebug "locate \"/$FQDN/drushrc.php\" :: SITEDIR=$SITEDIR"

if [ -d "$SITEDIR" ]; then
  echo "$SITEDIR"
else
  COUNT=$(echo "$SITEDIR" | wc -l)
  echodebug "COUNT=$COUNT"
  if [ $COUNT -gt 1 ]; then
    echoerr "$FQDN is ambiguous ($COUNT matches):"
    echoerr "$SITEDIR"
    exit 2
  else
    echoerr "$FQDN: could not find $FQDN/drushrc.php"
    exit 1
  fi
fi
